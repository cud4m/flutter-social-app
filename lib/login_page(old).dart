/*
//https://www.youtube.com/watch?v=efbB8-x9T2c
import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:path_provider/path_provider.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

import 'package:flutter_app/data/django_api.dart';

class LoginPage extends StatefulWidget {
  static String tag = 'login-page';

  @override
  LoginPageState createState() => LoginPageState();
}


class LoginPageState extends State<LoginPage>{
  final  usercontroller = TextEditingController();
  final  passcontroller = TextEditingController();

  @override
  void dispose() {
    // Clean up the controller when the Widget is disposed
    usercontroller.dispose();
    passcontroller.dispose();
    super.dispose();
  }
  @override
  Widget build(BuildContext context) {
    String user = "";
    String pass = "";


    final logo = Hero(
      tag: 'hero',
      child: CircleAvatar(
        backgroundColor: Colors.transparent,
        radius: 48.0,
        child: Icon(Icons.person_pin_circle),
        //child: Image.asset('path/here'),
      ),
    );

    final email = TextFormField(
      keyboardType: TextInputType.emailAddress,
      autofocus: false,
      initialValue: 'yourmailhere@examplemail.com',
      decoration: InputDecoration(
        hintText: 'Email',
        contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(32.0),
        )
      ),
    );

    final username = TextFormField(
      controller: usercontroller,
      onSaved: (String str){
        print("saved");
        setState(() {
          usercontroller.text = str;

        });
      },
      keyboardType: TextInputType.text,
      autofocus: false,
      //initialValue: 'yourname',
      decoration: InputDecoration(
          hintText: 'nickname',
          contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
          border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(32.0),
          )
      ),
      onFieldSubmitted: (String str) {
        setState(() {
          user = str;
        });
      },

    );


    final password = TextFormField(
      controller: passcontroller,
      onSaved: (String str){
        setState(() {
          passcontroller.text = str;
        });
      },
      autofocus: false,
      obscureText: true,
      decoration: InputDecoration(
          hintText: 'Password',
          contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
          border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(32.0),
          )
      ),
    );


    final loginButton = Padding(
      padding: EdgeInsets.symmetric(vertical: 16.0),
      child: Material(
        borderRadius: BorderRadius.circular(35.0),
        shadowColor: Colors.lime.shade100,
        elevation: 5.0,
        child: MaterialButton(onPressed: ()  {

          UserAuth().logIn(username.controller.text, password.controller.text);
        },
          color: Colors.yellow,
          child: Text('Log in', style: TextStyle(color: Colors.black45)),


        ),
      ),
    );

    final forgotLabel = FlatButton(
      child: Text(
        'Contraseña olvidada',
        style: TextStyle(color: Colors.black45),
      ),
      onPressed: () {
        // Todo implement logic
      },
    );
    return Scaffold(
      backgroundColor: Colors.white,
      body: Center(
        child: ListView(
          shrinkWrap: true,
          padding: EdgeInsets.only(left:24.0, right: 24.0),
          children: <Widget>[
            logo,

            SizedBox(height: 48.0),
            email,
            SizedBox(height: 8.0),
            username,
            SizedBox(height: 8.0),
            password,
            SizedBox(height: 48.0),
            loginButton,
            forgotLabel,
          ],
        )
      )
    );
  }
}

*/

