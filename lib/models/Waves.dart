
class Waves {
  final int id;
  final String name;
  final String short_description;
  final String media;
  final List support;
  final List tags;
  final List agitators;

  Waves.fromJsonMap(Map map)
      : id = map['id'],
        name = map['name'],
        short_description = map['short_description'],
        media = map['media'],
        support = map['support'],
        tags = map['tags'],
        agitators = map['agitators'];
//TODO timestamp
}