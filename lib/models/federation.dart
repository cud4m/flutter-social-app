class  Federation {

  final int id;
  final String name;
  final List<dynamic> tag;
  final List<dynamic> users;
  final List<dynamic> staff;


  Federation.fromJsonMap(Map map)
      :
        id = map['id'],
        name = map['name'],
        tag = map['tag'],
        users = map['users'],
        staff = map['staff'];

}


class  Thread {

 // final int id;
  final String name;
  final int federation;

Thread.fromJsonMap(Map map)
      :
        //id = map['id'],
        federation = map['federation'],
        name = map['name'];

}