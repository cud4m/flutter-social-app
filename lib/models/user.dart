

class User {
  String _username;
  String _token;
  User(this._username, this._token);

  User.map(dynamic obj) {
    this._username = obj["username"];
    this._token = obj["token"];
  }

  String get username => _username;
  String get password => _token;

  Map<String, dynamic> toMap() {
    var map = new Map<String, dynamic>();
    map["username"] = _username;
    map["token"] = _token;

    return map;
  }
}