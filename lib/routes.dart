import 'package:flutter/material.dart';
import 'package:flutter_app/screens/Friends.dart';
import 'package:flutter_app/screens/community/new_thread.dart';
import 'package:flutter_app/screens/community/new_wave.dart';
import 'package:flutter_app/screens/home_page.dart';
import 'package:flutter_app/screens/login/login_screen.dart';
import 'package:flutter_app/screens/login/signup_screen.dart';
import 'package:flutter_app/screens/settings.dart';
import 'main.dart';
import 'login_page(old).dart';
import 'package:flutter_app/screens/community.dart';
import 'package:flutter_app/data/django_api.dart';

final routes = {
  '/login':         (BuildContext context) => new LoginScreen(),
  '/signup':         (BuildContext context) => new SignUpScreen(),
  '/home':         (BuildContext context) => new HomePage(),
  '/add-friends':         (BuildContext context) => new AddFriends(),
  '/settings':         (BuildContext context) => new Settings(),
  '/new-thread':         (BuildContext context) => new NewThread(),
  '/new-wave':      (BuildContext context) => new NewWave(),
};