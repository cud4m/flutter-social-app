// Copyright 2018 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.
import 'dart:async';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_app/data/database_helper.dart';
import 'package:flutter_app/main.dart';
import 'package:http/http.dart' as http;

import 'package:simple_auth/simple_auth.dart' as simpleAuth;
import 'package:simple_auth_flutter/simple_auth_flutter.dart';

import 'package:flutter_app/data/django_api.dart';
import 'package:flutter_app/screens/community.dart';
import 'package:flutter_app/screens/home_page.dart';
import 'package:flutter_app/screens/trending_page.dart';

import 'package:flutter_app/auth.dart';
import 'package:flutter_app/screens/login/login_screen.dart';

import 'package:flutter_app/screens/community_page.dart';
import 'routes.dart';
import 'package:flutter_app/models/Waves.dart';
//https://medium.com/@kashifmin/flutter-login-app-using-rest-api-and-sqflite-b4815aed2149
//https://stackoverflow.com/questions/44121912/how-to-implements-a-scrollable-tab-on-the-bottom-screen#44122616
// https://github.com/Clancey/simple_auth/blob/master/simple_auth_flutter_example/lib/main.dart
/*
void main() {
  runApp(
    MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Y0ursense test',
      home: Scaffold(
        body: HomePage(),
      ),
      routes: <String, WidgetBuilder> {
        //"" : (BuildContext context) => new DetailPage()
      }
    ),
  );
}
*/


void main() {
  runApp(new TheApp());
}


class TheApp extends StatefulWidget {

  @override
  _TheAppState createState() =>  _TheAppState();
}

class _TheAppState extends State<TheApp> {

  @override
  initState() {
    super.initState();
    SimpleAuthFlutter.init(context);
  }



  @override
  Widget build (BuildContext context) {

    return new MaterialApp (
        debugShowCheckedModeBanner: true,
        title: 'Auth screen',
        theme: new ThemeData(
          primarySwatch: Colors.deepOrange,
        ),

        home: LoginScreen(),
        routes: routes,
    );
  }
}


class HomePage extends StatefulWidget {
  @override
  HomePageState createState() => HomePageState();

}

class HomePageState extends State<HomePage> with
  SingleTickerProviderStateMixin {

  TabController _tabController;
  ScrollController _scrollViewController;

  var url = 'http://192.168.1.243:8000/waves/';
  StreamController<Waves> streamController;
  List<Waves> waves_list = [];


  Future<String> getData() async {
    http.Response response = await http.get(
        Uri.encodeFull(url),
        headers: {

          "method": "GET"
        }
      //
    ); // response

    return 'Success';
  } // getData


  @override
  void initState() {
    super.initState();

    //TabBar Scrollview
    _tabController = TabController(length: 3, vsync: this);
    _scrollViewController = ScrollController();
  }



  @override
  void dispose() {
    super.dispose();
    streamController.close();
    streamController = null;

    _tabController.dispose();
    _scrollViewController.dispose();
  }


  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return new Scaffold(

      body: NestedScrollView(

          controller: _scrollViewController,
          headerSliverBuilder: (BuildContext context, bool boxIsScrolled) {
            return <Widget>[
              SliverAppBar(
                title: Text('Yoursense'),
                pinned: true,
                floating: true,
                forceElevated: boxIsScrolled,
                actions: <Widget>[
                  IconButton (

                    icon: Icon( Icons.settings),
                    onPressed: (){
                      Navigator.pushNamed(context, '/settings');
                    },
                  )
                ],
                bottom: TabBar(
                  tabs: <Widget>[
                    Tab(
                      icon: Icon(Icons.home),
                    ),
                    Tab(

                        icon: Image(image:  AssetImage("assets/trending.png"))
                    ),

                    Tab(

                      icon: Icon(Icons.people)
                    )
                  ],
                  controller: _tabController,
                ),

              ),

            ];
          },
          body: TabBarView(children: <Widget>[
            PageOne(),
            PageTwo(),
            CommunityPage()
          ],
          controller: _tabController,
          ),
      ),

    );
  }
}









/// InkWell class rectangulo menu rleta inferior