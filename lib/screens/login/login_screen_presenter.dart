import 'dart:async';

import 'package:flutter_app/data/django_api.dart';
import 'package:flutter_app/models/user.dart';

abstract class LoginScreenContract {
  void onLoginSuccess(User user);
  void onLoginError(String errorTxt);
}

class LoginScreenPresenter {
  LoginScreenContract _view;
  ApiInterface api = ApiInterface();
  LoginScreenPresenter(this._view);

  doLogin(String username, String password) {
    api.getToken(username, password).then((User user) {
      _view.onLoginSuccess(user);
    }).catchError((error) => _view.onLoginError(error.toString()));
  }
}


abstract class SignUpScreenContract {
  void onLoginSuccess(User user);
  void onLoginError(String errorTxt);
}

class SignUpScreenPresenter {
  SignUpScreenContract _view;
  ApiInterface api = ApiInterface();
  SignUpScreenPresenter(this._view);

  doSignUp(String username, String password, String email) {
    api.getTokenSignUp(username, password, email).then((User user) {
      _view.onLoginSuccess(user);
    }).catchError((error) => _view.onLoginError(error.toString()));
  }
}