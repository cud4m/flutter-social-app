import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter_app/auth.dart';
import 'package:flutter_app/data/database_helper.dart';
import 'package:flutter_app/models/user.dart';
import 'package:flutter_app/screens/login/login_screen_presenter.dart';

class SignUpScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return new SignUpScreenState();
  }
}

class SignUpScreenState extends State<SignUpScreen>
    implements SignUpScreenContract, AuthStateListener {
  BuildContext _ctx;

  bool _isLoading = false;
  final formKey = new GlobalKey<FormState>();
  final scaffoldKey = new GlobalKey<ScaffoldState>();
  String _username, _password, _email;

  SignUpScreenPresenter _presenter;

  SignUpScreenState() {
    _presenter = new SignUpScreenPresenter(this);
    var authStateProvider = new AuthStateProvider();
    authStateProvider.subscribe(this);
  }

  void _submit() {
    final form = formKey.currentState;

    if (form.validate()) {
      setState(() => _isLoading = true);
      form.save();
      _presenter.doSignUp(_username, _password, _email);
    }
  }

  void _showSnackBar(String text) {
    scaffoldKey.currentState
        .showSnackBar(new SnackBar(content: new Text(text)));
  }

  @override
  onAuthStateChanged(AuthState state) {

    if(state == AuthState.LOGGED_IN)
      Navigator.of(_ctx).pushReplacementNamed("/home");
  }

  @override
  Widget build(BuildContext context) {
    _ctx = context;

    var loginBtn   = new FlatButton(
        onPressed: () {
          Navigator.pushNamed(context, '/login');
        },
        child: Text("Already have an account? Log in!  ")
    );

    var signUpBtn = new RaisedButton(
      onPressed: _submit,
      child: new Text("SIGN UP"),
      color: Colors.primaries[0],
    );
    var signUpForm = new Column(
      children: <Widget>[
        new Text(
          "Sign up",
          textScaleFactor: 2.0,
        ),
        new Form(
          key: formKey,
          child: new Column(
            children: <Widget>[
              new Padding(
                padding: const EdgeInsets.all(8.0),
                child: new TextFormField(
                  onSaved: (val) => _username = val,
                  validator: (val) {
                    return  null;
                      //val.length < 10
                        //? "Username must have atleast 10 chars"
                        //: null;
                  },
                  decoration: new InputDecoration(labelText: "Username"),
                ),
              ),

              new Padding(
                padding: const EdgeInsets.all(8.0),
                child: new TextFormField(
                  onSaved: (val) => _email = val,
                  validator: (val) {
                    return  null;
                    //val.length < 10
                    //? "Username must have atleast 10 chars"
                    //: null;
                  },
                  decoration: new InputDecoration(labelText: "Email"),
                ),
              ),

              new Padding(
                padding: const EdgeInsets.all(8.0),
                child: new TextFormField(
                  onSaved: (val) => _password = val,
                  validator: (val) {
                    return
                      val.length < 8
                    ? "Username must have atleast 10 chars"
                    : null;
                  },
                  obscureText: true,
                  decoration: new InputDecoration(labelText: "Password"),
                ),
              ),
            ],
          ),
        ),
        _isLoading ? new CircularProgressIndicator() : signUpBtn
      ],
      crossAxisAlignment: CrossAxisAlignment.center,
    );

    return new Scaffold(
      appBar: null,
      key: scaffoldKey,
      body: new Container(
        decoration: new BoxDecoration(
          image: new DecorationImage(
              image: new AssetImage("assets/humming_bird.jpg"),
              fit: BoxFit.cover),
        ),
        child: new Center(
          child: new ClipRect(
            child: new BackdropFilter(
              filter: new ImageFilter.blur(sigmaX: 10.0, sigmaY: 10.0),
              child: new Container(
                child: Column(
                  children: <Widget>[
                    signUpForm,
                    loginBtn,


                  ],
                ),
                height: 600.0,
                width: 300.0,
                decoration: new BoxDecoration(
                    color: Colors.grey.shade200.withOpacity(0.5)),
              ),
            ),
          ),
        ),
      ),
    );
  }

  @override
  void onLoginError(String errorTxt) {
    _showSnackBar(errorTxt);
    setState(() => _isLoading = false);
  }

  @override
  void onLoginSuccess(User user) async {
    _showSnackBar(user.toString());
    setState(() => _isLoading = false);
    var db = new DatabaseHelper();
    await db.saveUser(user);
    var authStateProvider = new AuthStateProvider();
    authStateProvider.notify(AuthState.LOGGED_IN);
    authStateProvider.dispose(this);
  }
}