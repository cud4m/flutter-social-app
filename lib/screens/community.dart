
import 'dart:async';
import 'dart:convert';
import 'package:flutter_app/data/database_helper.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app/models/Waves.dart';
import 'package:flutter_app/models/api_settings.dart';
import 'package:flutter_app/models/federation.dart';
import 'package:http/http.dart' as http;
import 'package:flutter_app/screens/community_page.dart';

class Community extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemExtent: 250.0,
      itemBuilder: (context, index) =>
          Container(
            padding: EdgeInsets.all(10.0),
            child: Material(
              elevation: 4.0,
              borderRadius: BorderRadius.circular(5.0),
              color: index % 2 == 0 ? Colors.cyan : Colors.deepOrange,
              child: Center(
                child: Text(index.toString()),
              ),
            ),
          ),
    );
  }
}

class FederationWavesList extends StatefulWidget {
  FederationWavesList(this.federation_list);
  final int federation_list;
  @override
  FederationWavesListState createState() => FederationWavesListState(federation_list);
}

class FederationWavesListState extends State<FederationWavesList> {
  List<Thread> get_threads = [];
  final int federation_list;
  FederationWavesListState(this.federation_list);



  StreamController<Thread> streamThreadController;
  StreamController<Waves> streamWavesController;



  load(StreamController sc) async {
    print("thathsit many cre" + federation_list.toString() );

    print("working");
    String urlthreads = globalUrl +  'federation/'+  federation_list.toString() + "/getthreads/";
    String urlwaves = globalUrl +  'federation/';

    var client = new http.Client();

    var request = new http.Request('GET', Uri.parse(urlthreads));
    request.headers['Authorization'] = "Token " + await DatabaseHelper().getToken();
    print("url " + urlthreads.toString());
    var streamedRequest = await client.send(request);
    streamedRequest.stream
        .transform(utf8.decoder)
        .transform(json.decoder)
        .expand((anon_fun) => anon_fun)
        .map((map) => Thread.fromJsonMap(map))
        .pipe(streamThreadController);
    print("done");
  }


  @override
  void initState() {

   streamThreadController =
        StreamController.broadcast(); // subscribe to the streaming data
   streamThreadController.stream.listen(
            (streamed_data) => setState(() => get_threads.add(streamed_data)));
   load(streamThreadController);
    super.initState();

  }

  @override
  void dispose() {
    streamThreadController.close();
    streamThreadController = null;
    super.dispose();

  }

  @override
  Widget build(BuildContext context) {

    return new ListView.builder(
      itemBuilder: (BuildContext context, int index) {
        if (index >= get_threads.length ) {
          return null;
        }
        print("making card");
        return new Card(

            child: Column(children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[

                  Text(get_threads[index].name != null
                     ? get_threads[index].name.toString()
                   : 'null',
                 textAlign: TextAlign.center,),
                ],
              ),

              /*  Image.network(
                federation_list[index].media != null
                    ? federation_list[index].media
                    : 'not-found',
              ),*/
              DescriptionTextWidget(
                text: get_threads[index].name != null
                    ? get_threads[index].name.toString()
                    : 'null',
              ),
              new ButtonTheme.bar(
                // make buttons use the appropriate styles for cards
                  child: new ButtonBar(
                    children: <Widget>[
                      new IconButton(
                          icon: new Icon(Icons.favorite, color: Colors.yellowAccent),
                          onPressed: () {
                            Navigator.of(context).pushNamed("");
                          }),
                      new FlatButton(
                        child: const Text('SUPPORT'),
                        onPressed: () {/* ... */},
                      ),
                      new FlatButton(
                        child: const Text('AGITATE'),
                        onPressed: () {/* ... */},
                      ),
                    ],
                  ))
            ]));
      }, // itemBuilder,
    );

  }
}


class CommunityDetail extends StatefulWidget {
  CommunityDetail(this.federation_list);
  final int federation_list;
  @override
  CommunityDetailState createState() => new CommunityDetailState(federation_list);
}

class CommunityDetailState extends State<CommunityDetail> with SingleTickerProviderStateMixin {
  final int federation_list;
  CommunityDetailState(this.federation_list);

  ScrollController _scrollViewController;
  TabController _controller;

  @override
  void initState() {
    super.initState();
    _controller = new TabController(vsync: this, length: 2);
    _scrollViewController = ScrollController();


  }

  @override
  void dispose() {
    _controller.dispose();
    _scrollViewController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(

      body: NestedScrollView(

        controller: _scrollViewController,
        headerSliverBuilder: (BuildContext context, bool boxIsScrolled) {
          return <Widget>[
            SliverAppBar(
              title: Text("Community X"),
              centerTitle: true,
              snap: false,
              pinned: false,
              floating: false,
              forceElevated:boxIsScrolled,
              bottom: TabBar(
                tabs: <Widget>[
                  Tab(
                    icon: Icon(Icons.movie_creation),
                  ),
                  Tab(

                      icon: Icon(Icons.textsms)
                  ),
                ],
                controller: _controller,
              ),

            ),

          ];
        },
        body: TabBarView(children: <Widget>[
          Community(),
          FederationWavesList(federation_list),

        ],
          controller: _controller,
        ),
      ),

    );
  }
}





//https://stackoverflow.com/questions/49572747/flutter-how-to-hide-or-show-more-text-within-certain-length
class DescriptionTextWidget extends StatefulWidget {
  final String text;

  DescriptionTextWidget({@required this.text});

  @override
  _DescriptionTextWidgetState createState() =>
      new _DescriptionTextWidgetState();
}

class _DescriptionTextWidgetState extends State<DescriptionTextWidget> {
  String firstHalf;
  String secondHalf;

  bool flag = true;

  @override
  void initState() {
    super.initState();

    if (widget.text.length > 250) {
      firstHalf = widget.text.substring(0, 250);
      secondHalf = widget.text.substring(250, widget.text.length);
    } else {
      firstHalf = widget.text;
      secondHalf = "";
    }
  }

  @override
  Widget build(BuildContext context) {
    return new Container(
      padding: new EdgeInsets.symmetric(horizontal: 10.0, vertical: 10.0),
      child: secondHalf.isEmpty
          ? new Text(firstHalf)
          : new Column(
        children: <Widget>[
          new Text(flag ? (firstHalf + "...") : (firstHalf + secondHalf)),
          new InkWell(
            child: new Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: <Widget>[
                new Text(
                  flag ? "show more" : "show less",
                  style: new TextStyle( color: Theme.of(context).primaryColor,),
                ),
              ],
            ),
            onTap: () {
              setState(() {
                flag = !flag;
              });
            },
          ),
        ],
      ),
    );
  }
}
