import 'package:flutter/material.dart';
import 'package:flutter_app/data/database_helper.dart';
import 'package:flutter_app/routes.dart';




class Settings  extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Settings"),
      ),
      body:
      Center(
          child: RaisedButton(
              child: Text("Log out"),
              onPressed: () {
            DatabaseHelper().deleteUsers();
            Navigator.pushNamed(context, '/login');
          }),
        ),
    );
  }
}