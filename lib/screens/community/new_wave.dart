
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_multiple_image_picker/flutter_multiple_image_picker.dart';



class NewWave extends StatefulWidget {
  @override
  NewWaveState createState() => NewWaveState();
}
class NewWaveState  extends State<NewWave> {
  final _wavecontroller = TextEditingController();


  BuildContext context;
  String _platformMessage = 'No Error';
  List images;
  int maxImageNo = 10;
  bool selectSingleImage = false;



  @override
  void dispose() {
    // TODO: implement dispose
    _wavecontroller.dispose();
    super.dispose();
  }


  initMultiPickUp() async {
    setState(() {
      images = null;
      _platformMessage = 'No Error';
    });
    List resultList;
    String error;
    try {
      resultList = await FlutterMultipleImagePicker.pickMultiImages(
          maxImageNo, selectSingleImage);
    } on PlatformException catch (e) {
      error = e.message;
    }

    if (!mounted) return;

    setState(() {
      images = resultList;
      if (error == null) _platformMessage = 'No Error Dectected';
    });
  }


  @override
  Widget build(BuildContext context) {

    final waveName = TextFormField(
      controller: _wavecontroller,
      maxLengthEnforced: true,
      inputFormatters:[
        LengthLimitingTextInputFormatter(100)
      ] ,
      decoration: InputDecoration(
        hintText: "Name"
      ),

    );

    final waveDescription = TextFormField(
      controller: _wavecontroller,
      inputFormatters:[
        LengthLimitingTextInputFormatter(666)
      ] ,
      decoration: InputDecoration(
          hintText: "Wave Description"
      ),

    );

    final addButton = FlatButton( child: Text("Add Waves"),
        onPressed: () {
          initMultiPickUp;
          //TODO
        });

    final dropBtn =  new DropdownButton<String>(
        items: <String>['Foo', 'Bar'].map((String value) {
          return new DropdownMenuItem<String>(
            value: value,
            child: new Text(value),
          );
        }).toList(),
        onChanged: (_) {},
    );


    return Scaffold(
      appBar: AppBar(
        title: Text("Create Wave"),
      ),
      body:
      Center(
        child: ListView(
          shrinkWrap: true,
          padding: EdgeInsets.only(left:24.0, right: 24.0),
          children: <Widget>[
            //new Image.file(File(imagePath))
            dropBtn,
            waveName,
            waveDescription,

          new Container(
            padding: const EdgeInsets.all(8.0),
            child: new Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                images == null
                    ? new Container(
                  height: 300.0,
                  width: 400.0,
                  child: new Icon(
                    Icons.image,
                    size: 250.0,
                    color: Theme.of(context).primaryColor,
                  ),
                )
                    : new SizedBox(
                  height: 300.0,
                  width: 400.0,
                  child: new ListView.builder(
                    scrollDirection: Axis.horizontal,
                    itemBuilder: (BuildContext context, int index) =>
                    new Padding(
                      padding: const EdgeInsets.all(5.0),
                      child: new Image.file(
                        new File(images[index].toString()),
                      ),
                    ),
                    itemCount: images.length,
                  ),
                ),
                new Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: new Text('Error Dectected: $_platformMessage'),
                ),
                new RaisedButton.icon(
                    onPressed: initMultiPickUp,
                    icon: new Icon(Icons.image),
                    label: new Text("Pick-Up Images")),
              ],
            ),
          ),

            addButton

          ],
        )
      ),
    );
  }
}