import 'package:flutter/material.dart';



class AddContent extends StatefulWidget {
  @override
  AddContentState createState() => new AddContentState();
}

class AddContentState extends State<AddContent> with SingleTickerProviderStateMixin {

  ScrollController _scrollViewController;
  TabController _controller;

  @override
  void initState() {
    super.initState();
    _controller = new TabController(vsync: this, length: 2);
    _scrollViewController = ScrollController();
  }

  @override
  void dispose() {
    _controller.dispose();
    _scrollViewController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      body: NestedScrollView(

        controller: _scrollViewController,
        headerSliverBuilder: (BuildContext context, bool boxIsScrolled) {
          return <Widget>[
            SliverAppBar(
              title: Text("Create new content"),
              centerTitle: true,
              snap: false,
              pinned: false,
              floating: false,
              forceElevated:boxIsScrolled,
              bottom: TabBar(
                tabs: <Widget>[
                  Tab(
                    icon: Icon(Icons.movie_creation),
                  ),
                  Tab(

                      icon: Icon(Icons.textsms)
                  ),
                ],
                controller: _controller,
              ),

            ),

          ];
        },
        body: TabBarView(children: <Widget>[


        ],
          controller: _controller,
        ),
      ),

    );
  }
}