import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_app/data/database_helper.dart';
import 'dart:async';
import 'package:http/http.dart' as http;
import 'package:flutter_app/data/django_api.dart';

import 'package:flutter_app/screens/community.dart';

import 'package:flutter_app/models/api_settings.dart';
import 'package:flutter_app/models/federation.dart';

import 'dart:math' as math;



int get_federation_thread;

// https://stackoverflow.com/questions/46480221/flutter-floating-action-button-with-speed-dail
// https://stackoverflow.com/questions/49781227/flutter-update-bottomnavigationbar
class CommunityPage extends StatefulWidget {
  @override
 CommunityPageState createState() => CommunityPageState();
}

class CommunityPageState extends State<CommunityPage> with TickerProviderStateMixin {
  List<Federation>  get_federation_list = [];
  StreamController<Federation> streamController;

  AnimationController _controllerFloatingMenu;
  static const List<IconData> icons = const [ Icons.list, Icons.theaters, ];

  TabController controller;



  load(StreamController sc) async {
    String url = globalUrl +  'federation/';
    var client = new http.Client();

    var request = new http.Request('GET', Uri.parse(url));
    request.headers['Authorization'] = "Token " + await DatabaseHelper().getToken();
    //print(request.headers);
    var streamedRequest = await client.send(request);
    streamedRequest.stream
        .transform(utf8.decoder)
        .transform(json.decoder)
        .expand((anon_fun) => anon_fun)
        .map((map) => Federation.fromJsonMap(map))
        .pipe(streamController);
  }


  @override
  void initState() {
    super.initState();

    //Fetch api data
    streamController =
        StreamController.broadcast(); // subscribe to the streaming data
    streamController.stream.listen(
            (streamed_data) => setState(() => get_federation_list.add(streamed_data)));
    load(streamController);
    controller = new TabController(vsync: this, length: get_federation_list.length);

      _controllerFloatingMenu = new AnimationController(
      vsync: this,
      duration: const Duration(milliseconds: 500),
    );

  }

  @override
  void dispose() {
    _controllerFloatingMenu.dispose();
    controller.dispose();
    streamController.close();
    streamController = null;

    super.dispose();

  }

  @override
  Widget build(BuildContext context) {
    int _screen = 0;
    return Center(
        child: Scaffold(


            floatingActionButton: new Column(
              mainAxisSize: MainAxisSize.min,
              children: new List.generate(icons.length, (int index) {
                Widget child = new Container(
                  height: 70.0,
                  width: 56.0,
                  alignment: FractionalOffset.topCenter,
                  child: new ScaleTransition(
                    scale: new CurvedAnimation(
                      parent: _controllerFloatingMenu,
                      curve: new Interval(
                          0.0,
                          1.0 - index / icons.length / 2.0,
                          curve: Curves.easeOut
                      ),
                    ),
                    child: new FloatingActionButton(
                      heroTag: null,
                      backgroundColor: Theme.of(context).primaryColor,
                      mini: true,
                      child: new Icon(icons[index]),
                      onPressed: () {
                        Navigator.pushNamed(context, index==0 ? "/new-thread" : "/new-wave");
                      },
                    ),
                  ),
                );
                return child;
              }).toList()..add(
                new FloatingActionButton(
                  heroTag: null,
                  child: new AnimatedBuilder(
                    animation: _controllerFloatingMenu,
                    builder: (BuildContext context, Widget child) {
                      return new Transform(
                        transform: new Matrix4.rotationZ(_controllerFloatingMenu.value * 0.5 * math.PI),
                        alignment: FractionalOffset.center,
                        child: new Icon(_controllerFloatingMenu.isDismissed ? Icons.add : Icons.close),
                      );
                    },
                  ),
                  onPressed: () {
                    if (_controllerFloatingMenu.isDismissed) {
                      _controllerFloatingMenu.forward();
                    } else {
                      _controllerFloatingMenu.reverse();
                    }
                  },
                ),
              ),
            ),


          bottomNavigationBar: new Row(
          mainAxisSize: MainAxisSize.max,
          crossAxisAlignment: CrossAxisAlignment.center,

          children: [
            Expanded(
              child:
              new AnimatedCrossFade(
                firstChild: new Material(

                  color: Theme.of(context).primaryColor,
                  child: new TabBar(
                    controller:
                    TabController(length: get_federation_list.length, vsync: this),
                    isScrollable: true,
                    tabs:

                    new List.generate(get_federation_list.length, (index) {
                      return new Tab(text: get_federation_list[index].name.toUpperCase() );
                    }),

                  ),
                ),
                secondChild: new Container(  ),
                crossFadeState: _screen == 0
                    ? CrossFadeState.showFirst
                    : CrossFadeState.showSecond,
                duration: const Duration(milliseconds: 300),
                
              ),
            )
            
          ]),
      body: TabBarView(
        controller: controller ,
        children:  new List.generate(get_federation_list.length, (index) {
          get_federation_thread = get_federation_list[index].id;
          print("id" + get_federation_thread.toString());
          CommunityDetail(get_federation_thread);
          return new CommunityDetail(get_federation_thread);
        }),
        )
    ));
  }
}





// Displays one Entry. If the entry has children then it's displayed
// with an ExpansionTile.
