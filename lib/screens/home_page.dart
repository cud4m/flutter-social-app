import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_app/data/database_helper.dart';
import 'package:flutter_app/models/api_settings.dart';
import 'dart:async';
import 'package:http/http.dart' as http;
import 'package:flutter_app/data/django_api.dart';
import 'package:flutter_app/models/Waves.dart';


class PageOne extends StatefulWidget {
  @override
  PageOneState createState() => PageOneState();
}

class PageOneState extends State<PageOne> with TickerProviderStateMixin {
  List data;
  StreamController<Waves> streamController;
  List<Waves> waves_list = [];

  load(StreamController sc) async {
    String url = globalUrl + 'waves/';
    var client = new http.Client();

    var request = new http.Request('GET', Uri.parse(url));
    request.headers['Authorization'] = "Token " + await DatabaseHelper().getToken();
    print(request.headers.toString());
    var streamedRequest = await client.send(request);
    streamedRequest.stream
        .transform(utf8.decoder)
        .transform(json.decoder)
        .expand((anon_fun) => anon_fun)
        .map((map) => Waves.fromJsonMap(map))
        .pipe(streamController);
  }

  @override
  void initState() {
    super.initState();

    //Fetch api data
    streamController =
        StreamController.broadcast(); // subscribe to the streaming data
    streamController.stream.listen(
        (streamed_data) => setState(() => waves_list.add(streamed_data)));
    load(streamController);
  }

  @override
  void dispose() {
    super.dispose();
    streamController.close();
    streamController = null;
  }

  @override
  Widget build(BuildContext context) {
    int _screen = 0;
    return Center(
        child: Scaffold(

          floatingActionButton: FloatingActionButton(
            child:  Icon(Icons.receipt),
            onPressed: () {
              //TODO añadir amigos
            },

          ),
      body: new ListView.builder(
        itemBuilder: (BuildContext context, int index) {
          if (index >= waves_list.length) {
            return null;
          }
          return new Card(
              child: Column(children: <Widget>[
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[

                    Text(waves_list[index].name != null
                        ? waves_list[index].name
                        : 'null',
                        textAlign: TextAlign.center,),
                  ],
                ),

            Image.network(
              waves_list[index].media != null
                  ? waves_list[index].media
                  : 'not-found',
            ),
            DescriptionTextWidget(
              text: waves_list[index].short_description != null
                  ? waves_list[index].short_description
                  : 'null',
            ),
            new ButtonTheme.bar(
                // make buttons use the appropriate styles for cards
                child: new ButtonBar(
              children: <Widget>[
                new IconButton(
                    icon: new Icon(Icons.favorite, color: Colors.yellowAccent),
                    onPressed: () {
                      Navigator.of(context).pushNamed("");
                    }),
                new FlatButton(
                  child: const Text('SUPPORT'),
                  onPressed: () {/* ... */},
                ),
                new FlatButton(
                  child: const Text('AGITATE'),
                  onPressed: () {/* ... */},
                ),
              ],
            ))
          ]));
        }, // itemBuilder,
      ),
    ));
  }
}

//https://stackoverflow.com/questions/49572747/flutter-how-to-hide-or-show-more-text-within-certain-length
class DescriptionTextWidget extends StatefulWidget {
  final String text;

  DescriptionTextWidget({@required this.text});

  @override
  _DescriptionTextWidgetState createState() =>
      new _DescriptionTextWidgetState();
}

class _DescriptionTextWidgetState extends State<DescriptionTextWidget> {
  String firstHalf;
  String secondHalf;

  bool flag = true;

  @override
  void initState() {
    super.initState();

    if (widget.text.length > 250) {
      firstHalf = widget.text.substring(0, 250);
      secondHalf = widget.text.substring(250, widget.text.length);
    } else {
      firstHalf = widget.text;
      secondHalf = "";
    }
  }

  @override
  Widget build(BuildContext context) {
    return new Container(
      padding: new EdgeInsets.symmetric(horizontal: 10.0, vertical: 10.0),
      child: secondHalf.isEmpty
          ? new Text(firstHalf)
          : new Column(
              children: <Widget>[
                new Text(flag ? (firstHalf + "...") : (firstHalf + secondHalf)),
                new InkWell(
                  child: new Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: <Widget>[
                      new Text(
                        flag ? "show more" : "show less",
                        style: new TextStyle( color: Theme.of(context).primaryColor,),
                      ),
                    ],
                  ),
                  onTap: () {
                    setState(() {
                      flag = !flag;
                    });
                  },
                ),
              ],
            ),
    );
  }
}

// Displays one Entry. If the entry has children then it's displayed
// with an ExpansionTile.
