import 'package:flutter/material.dart';
import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:http/http.dart' as http;
import 'package:path_provider/path_provider.dart';
import 'package:flutter_app/data/database_helper.dart';



import 'package:flutter_app/utils/network_util.dart';
import 'package:flutter_app/models/user.dart';

class RestDatasource {
  NetworkUtil _netUtil = new NetworkUtil();
  static final BASE_URL = "http://192.168.1.243:8000";
  static final LOGIN_URL = BASE_URL + "/auth/token/create/";
  //static final _API_KEY = "somerandomkey";

  Future<User> login(String username, String password) {
    return _netUtil.post(LOGIN_URL, body: {
      //"token": _API_KEY,
      "username": username,
      "password": password
    }).then((dynamic res) {
      print(res.toString());
      if (res["non_field_errors"]) throw new Exception("Not valid credentials provided");
      return new User.map(res["user"]);
    });
  }
}



class ApiInterface {
  var url  = 'http://192.168.1.243:8000';
  NetworkUtil _netUtil = new NetworkUtil();


  Future<User> getToken(username, password) async { // check dynamic use why is this type

    Map<String, dynamic> tokendata;

    var response = await http.post(
        Uri.encodeFull(url + '/auth/token/create/'),
        body :{
          "password": password,
          "username": username
        },

    );
    print(password + "and" + username);
    tokendata = json.decode(response.body,);
    String token = tokendata['auth_token'];
    if (token !=null) { //TODO handle error when not server connection beautifull message
      return User(username, token);
    }
    throw new Exception("Not valid credentials");

    }



  Future<User> getTokenSignUp(username, password, email) async { // check dynamic use why is this type

    Map<String, dynamic> tokendata;

    var response = await http.post(
      Uri.encodeFull(url + '/auth/users/create/'),
      body :{
        "email" : email,
        "password": password,
        "username": username
      },

    );
    print(password + "and" + username);
    tokendata = json.decode(response.body,);
    String token = tokendata['auth_token'];
    if (token !=null) { //TODO handle error when not server connection beautifull message
      return User(username, token);
    }
    throw new Exception("Not valid credentials");

  }



  Future<http.Response> getData(endpoint) async {
    http.Response response = await http.get(
        Uri.encodeFull(url + endpoint),
        headers: {
          "Authorization": "Token " + await DatabaseHelper().getToken(),
          "method": "GET"
        }
      //
    ); // response


    return response;
  } // getData

}






/*
class AppStorage {

  Future<String> get _localPath async {
    final directory = await getApplicationDocumentsDirectory();

    return directory.path;
  }

  Future<File> get _localFile async {
    final path = await _localPath;
    return File('$path/token.txt');
  }

  Future<String> readToken() async {
    try {
      print("file?");
      final file = await _localFile;

      // Read the file
      String contents = await file.readAsString();
      print("content" + contents.toString());
      return contents.toString();
    } catch (e) {
      // If we encounter an error, return 0
      return "Error";
    }
  }

  Future<File> writeToken(String token) async {
    final file = await _localFile;
    print("token " + token.toString());

    // Write the file
    return file.writeAsString('$token');
  }

  Future<File> deleteToken() async {
    final file = await _localFile;


    // Remove the file
    return file.delete();
  }
}*/

/*
class UserAuth {

  Future logIn(String username, String password) async {
    //Get user token
    String token = await ApiInterface().getToken(username, password);
    // Save User token
    if (token != "No valid login Credentials") {
      AppStorage().writeToken(token); // Future<String> toString????
    }
  }

  logOut()  {
    AppStorage().deleteToken();
  }

  bool isLoggedIn ()  {
    var thereistoken = true;
    try {

      print("try");
      AppStorage().readToken().then((value) {
        if (value != "Error") {
          thereistoken = true;
          print("true");
        }
        else {
          thereistoken = false;
          print("false");

        }
      });
    }
    catch (e) {
      thereistoken = false;
    }
    print("theristoken" + thereistoken.toString());
    return thereistoken;

  }
}*/